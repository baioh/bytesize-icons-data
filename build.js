const fs = require('fs')
const path = require('path')


const iconsPath = './node_modules/bytesize-icons/dist/icons'
const iconList = fs.readdirSync(iconsPath)

let icons = iconList.map((filename) => {
  const data = fs.readFileSync(path.join(iconsPath, filename), 'utf8')
    .replace(/<svg([^>]+)>/, '')
    .replace(`</svg>`, '')
    .trim()
    .split('\n')
    .map(line => line.trim())
    .join('')
  const name = filename.split('.')[0]
  console.log(name)
  return `  "${name}": \`${data}\``
}).join(',\n')

icons = `module.exports = {
${icons}
}`

fs.writeFileSync('./icons.js', icons, 'utf8')
